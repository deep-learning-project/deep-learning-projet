import requests
import numpy as np
import streamlit as st
from numpy import asarray

def main():
    st.title("Document Type Detection App")
    st.markdown("Upload your document, the application will try to determine the type of document.")

    uploaded_file = st.file_uploader("Choose a file")
    if uploaded_file is not None:
        # To read file as bytes:
        bytes_data = uploaded_file.getvalue()

        if st.button("Predict"):
            prediction = get_prediction(bytes_data)
            display_prediction(prediction)

def get_prediction(bytes_data):
    response = requests.post("http://serving-api:8080/predict", files={'file': ('test.png', bytes_data)}).json()
    return response.get("prediction", [])

def display_prediction(prediction):
    doc_types = {
        "0": "letter",
        "1": "form",
        "2": "email",
        "3": "handwritten",
        "4": "advertisement",
        "5": "scientific report",
        "6": "scientific publication",
        "7": "specification",
        "8": "file folder",
        "9": "news article",
        "10": "budget",
        "11": "invoice",
        "12": "presentation",
        "13": "questionnaire",
        "14": "resume",
        "15": "memo"
    }
    if prediction:
        prediction_index = prediction[0]
        prediction_text = doc_types.get(prediction_index)
        st.write(f"This document seems to be a {prediction_text}.")
    else:
        st.write("No prediction available.")

if __name__ == "__main__":
    main()

import torch
import lightning as L
import torch.nn as nn

class LitJQOVResNetClassifier(L.LightningModule):
    def __init__(self, dim_in:int, dim_out:int, lr:float, residual:bool=True, dim_in_last_layer = 512*32*32//4, optimizer=torch.optim.Adam):
        super().__init__()
        self.save_hyperparameters()

        cnn_conf = [[1, 64, 7], [6, 64, 3], [8, 128, 3], [12, 256, 3], [6, 512, 3]]

        layers = []

        for index, conf in enumerate(cnn_conf):
            nb_layer, nb_kernel, kernel_size = conf
            for i in range(nb_layer):
                seq = []
                seq.append(nn.Conv2d(dim_in, nb_kernel, kernel_size=kernel_size, stride=2 if i == 0 and index > 1 else 1, padding=(kernel_size-1)//2))
                seq.append(nn.BatchNorm2d(nb_kernel))
                seq.append(nn.ReLU())
                if index == 0:
                    seq.append(nn.MaxPool2d(kernel_size=2, stride=2))
                layers.append(nn.Sequential(*seq))
                dim_in = nb_kernel

        self.lr = lr
        self.cnn_layers = nn.ModuleList(layers)
        self.dim_in_last_layer = dim_in_last_layer
        self.max_pooling = nn.MaxPool2d(kernel_size=2)
        self.flatten = nn.Flatten()
        self.fc = nn.Linear(dim_in_last_layer, dim_out, bias=True)
        self.optimizer_builder = optimizer
        self.residual = residual

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self.cnn_layers[0](x)

        if self.residual:
            res = x

        for i in range((len(self.cnn_layers)-1) // 2):
            if self.residual:
                x = x + res

            for j in range(2):
                x = self.cnn_layers[1+2*i+j](x)

            res = x

        x = x + res

        x = self.max_pooling(x)

        x = self.flatten(x)

        x = self.fc(x)

        return x

    def training_validation_test_step(self, batch, batch_idx, mode='train'):
        x, y = batch
        y_pred = self(x)

        loss = nn.functional.cross_entropy(input=y_pred, target=y)
        acc = my_accuracy(y=y_pred, y_true=y)

        self.log(mode + '_loss', loss.item(), on_epoch=True, prog_bar=True, sync_dist=True)
        self.log(mode + '_acc', acc.item(), on_epoch=True, prog_bar=True, sync_dist=True)

        return loss

    def training_step(self, batch, batch_idx):
        return self.training_validation_test_step(batch, batch_idx, 'train')

    def validation_step(self, batch, batch_idx):
        return self.training_validation_test_step(batch, batch_idx, 'val')

    def test_step(self, batch, batch_idx):
        return self.training_validation_test_step(batch, batch_idx, 'test')

    def configure_optimizers(self):
        return self.optimizer_builder(self.parameters(), lr=self.lr)
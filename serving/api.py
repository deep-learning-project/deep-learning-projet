import os
import subprocess
from typing import Annotated
import pandas as pd
import numpy as np

import torch

from fastapi import FastAPI, File, UploadFile, Request, Form

from PIL import Image, ImageOps
from io import BytesIO

from ResNetClassifier import LitJQOVResNetClassifier

app = FastAPI()
call_save_counter = 0

# Load your machine learning prediction model
prediction_model = LitJQOVResNetClassifier.load_from_checkpoint("../artifacts/residual_best_model.ckpt")

def preprocess_image(image_bytes):
    image = Image.open(BytesIO(image_bytes))
    image = image.convert('L')  # Convert the image to grayscale
    
    img = ImageOps.pad(image, (500, 500), color='white')
    img = np.array(img)
    return img / 255

# Function to predict using the linear model DataFrame
def predict_with_model(image_bytes):
    model_input = torch.tensor(preprocess_image(image_bytes), dtype=torch.float).unsqueeze(0)
    prediction_result = torch.argmax(prediction_model(model_input), dim=1).item()
    print(prediction_result)
    return prediction_result

# Endpoint to predict the target value from an image
@app.post("/predict")
async def predict(file: UploadFile):
    image_bytes = await file.read()
    prediction = predict_with_model(image_bytes)
    return {"filename": file.filename, "prediction": [str(prediction)]}

# Deep Learning Project

This project is a deep learning project implemented in Python. It consists of a notebook that contains a deep learning model and algorithms.

## Project Structure

The project is organized as follows:

- `TrainResNe.ipynb`: The main notebook file that contains the implementation of the deep learning models.
- `data/`: A directory that contains the dataset used for training and testing the models.
- `results/`: A directory that contains the evaluation results and performance metrics.
- `app/`: A directory that contains the web application.

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository: `git clone https://github.com/your-username/deep-learning-project.git`
2. Install the required dependencies: `pip install -r requirements.txt`
3. Open the `TrainResNet.ipynb` file in Jupyter Notebook or any other compatible environment.
4. Run the notebook cells to train and evaluate the deep learning models.

## API and Web App

The API and the Web App are then launched. The API loads the pickle files, while the Web App part allows downloading an image representing a document. The website handles sending the image to the API, which returns a prediction. The API processes the image in the same way as the model.

To launch these two servers, you need to enter the command `docker compose up` in a command prompt shell opened within their respective folders.

## Contributing

Contributions to this project are welcome. If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.
